# Key-Booter  <img width="60px" src="https://media.giphy.com/media/WUlplcMpOCEmTGBtBW/giphy.gif" />

#### 🗒️ Description

Bienvenue sur le dépôt GitLab du projet **Key-Booter**. !

**Key-Booter** est un outil permettant de créer des clés USB amorçables. L'outil se veut simple à prendre en main.

| 🔢 Version | 📅 Date de sortie | 🔗 Lien de téléchargement |
| :-----: | :-----: | :-----: |
| *v1.0* | *Mars/2024* | https://gitlab.com/SuijoART/key-booter/-/releases/v1.0 |

| Captures d'écran | |
| :----: | :----: |
| [<img src="./screenshot/img00.png" width="320" height=240/>](./screenshot/img00.png) | [<img src="./screenshot/img01.png" width="320" height=240/>](./screenshot/img01.png) |

#### 📦 Pré-requis

Le programme nécessite les paquets suivants pour fonctionner : **wget**, **pv**, **dd** et **lsblk**

Pour les installer, exécuter les commandes suivantes :

```bash
sudo apt update
sudo apt install wget pv dd lsblk
```

#### 🛠️ Utilisation

Depuis la racine du projet, effectuer la commande suivante pour lancer l'exécutable :

```bash
sudo bash ./key-booter.sh
```

#### ℹ️ Informations complémentaires

Pour toute suggestion/modification n'hésitez pas à créer une issue/merge request.
