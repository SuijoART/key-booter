#!/bin/bash

###################################################### FUNCTIONS ######################################################

# ----------------- Affichage des crédits -----------------
function credits_displaying {
	echo -e "\n------------------------------------------------------------------------------------\n"
	echo -e "				      By SuijoART"
	echo -e	"					  FOR"
	echo -e "				   ESIEA KPS 2023/2024"
	echo -e "\n------------------------------------------------------------------------------------\n"
}

# ----------------- Affichage du menu -----------------
function menu_displaying {
	clear

	echo -e "\n------------------------------------------------------------------------------------\n"

	echo -e "		▄ •▄ ▄▄▄ . ▄· ▄▌    ▄▄▄▄·             ▄▄▄▄▄▄▄▄ .▄▄▄  "
	echo -e "		█▌▄▌▪▀▄.▀·▐█▪██▌    ▐█ ▀█▪▪     ▪     •██  ▀▄.▀·▀▄ █·"
	echo -e "		▐▀▀▄·▐▀▀▪▄▐█▌▐█▪    ▐█▀▀█▄ ▄█▀▄  ▄█▀▄  ▐█.▪▐▀▀▪▄▐▀▀▄ "
	echo -e "		▐█.█▌▐█▄▄▌ ▐█▀·.    ██▄▪▐█▐█▌.▐▌▐█▌.▐▌ ▐█▌·▐█▄▄▌▐█•█▌"
	echo -e "		·▀  ▀ ▀▀▀   ▀ •     ·▀▀▀▀  ▀█▄▀▪ ▀█▄▀▪ ▀▀▀  ▀▀▀ .▀  ▀"

	echo -e "\n---------------------------------------[MENU]---------------------------------------\n"

	echo -e "	Quelle distribution GNU/Linux voulez-vous installer ?\n"
	echo -e "                               1 - Ubuntu"
	echo -e "                               2 - Lubuntu"
	echo -e "                               3 - Pop!_OS"
	echo -e "                               4 - Manjaro"
	echo -e "                               5 - Debian\n"
	read -p "                       >>> Votre choix : " USER_CHOICE
}

# ----------------- Téléchargement de l'ISO -----------------
function iso_downloading {
	if [[ ${USER_CHOICE} -ge 1 && ${USER_CHOICE} -le 5 ]]
	then
		echo -e "\n             ---------------------------------------\n"
		echo -e "	[INFORMATION] Démarrage du processus de téléchargement de l'ISO ..."
		wget --quiet --show-progress --progress=dot:giga ${DISTRIBUTION_LINUX[${USER_CHOICE}]} --output-document ${ISO_NAME[${USER_CHOICE}]}

		# Vérification
		if [[ $? -eq 0 ]]
		then
			echo -e "\n\n	[SUCCÈS] Processus de téléchargement de l'ISO ${ISO_NAME[${USER_CHOICE}]} terminé ..."
		else
			echo -e "\n	[ERREUR] Consulter le fichier 'erreur.txt' pour plus de détails !"
			credits_displaying
			exit
		fi
	else
		echo -e "\n	[ERREUR] Choix non valide !"
		credits_displaying
		exit
	fi
}

# ----------------- Démarrage du processus de copie de données dans la clé USB -----------------
function usb_flashing {
	echo -e "\n             ---------------------------------------\n"
	echo -e "	[INFORMATION] Démarrage du processus de flash ...\n"
	echo -e "	[AVERTISSEMENT] Ne débrancher pas la clé USB !\n"

	pv -tprebw 80 ${ISO_NAME[${USER_CHOICE}]} | sudo dd of=${USB_DRIVE_PATH} bs=1M 1> /dev/null 2> erreur.txt && sync

	# Vérification
	if [ $? -eq 0 ]
	then
		echo -e "\n	[SUCCÈS] Processus de copie des données terminé ..."
		echo -e "\n	[INFORMATION] Vous pouvez maintenant retirer la clé USB ..."
		credits_displaying
	else
		echo -e "\n	[ERREUR] Consulter le fichier 'erreur.txt' pour plus de détails !"
		credits_displaying
	fi
}

###################################################### MAIN ######################################################

# ----------------- Initialisation des variables -----------------

USER_CHOICE=NULL

USB_DRIVE_PATH=$(lsblk -o PATH,NAME,RM,SIZE,RO,TYPE,MOUNTPOINT,VENDOR | grep "USB" | cut -d " " -f 1)

declare -A ISO_NAME=(
	["1"]="ubuntu.iso"
	["2"]="lubuntu.iso"
	["3"]="pop_os.iso"
	["4"]="manjaro.iso"
	["5"]="debian.iso"
)

declare -A DISTRIBUTION_LINUX=(
	["1"]="https://releases.ubuntu.com/22.04.4/ubuntu-22.04.4-desktop-amd64.iso"
	["2"]="https://cdimage.ubuntu.com/lubuntu/releases/22.04.4/release/lubuntu-22.04.4-desktop-amd64.iso"
	["3"]="https://iso.pop-os.org/22.04/amd64/intel/39/pop-os_22.04_amd64_intel_39.iso"
	["4"]="https://download.manjaro.org/gnome/23.1.3/manjaro-gnome-23.1.3-240113-linux66.iso"
	["5"]="https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.5.0-amd64-netinst.iso"
)

# ----------------- Exécution des fonctions -----------------

menu_displaying
iso_downloading
usb_flashing
